import logo from './logo.svg';
import './App.css';
import logox from "./logo-x.jpeg"
import zeus from "./assets/img/zeus.gif"
import commingSoon from "./assets/img/comming-soon.gif"
import gacorText from "./assets/img/slot-gacor.gif"
function App() {
  return (
    <div className="App" style={{backgroundColor: 'black', height: '100vh'}}>
     <div>
        <img width="180" src={zeus} />
      </div>
      <div style={{marginTop: 25, marginBottom: 40}}>
        <img width="180" src={commingSoon} />
      </div>
      <div>
        <img width="280" src={gacorText} />
      </div>
    </div>
  );
}

export default App;
